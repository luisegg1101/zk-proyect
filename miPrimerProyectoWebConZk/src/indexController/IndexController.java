package indexController;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timer;
import org.zkoss.zul.Window;

import chatUtil.MensajeChat;

public class IndexController extends SelectorComposer<Component> implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Wire
	Window miVentana;
	
	@Wire
	Button btnEnviar;
	
	@Wire
	Textbox tvHistorial, txtMensaje, txtUsuario;
	
	@Wire
	Timer timer;
	
	MensajeChat mc;
	String mensaje="";
	String usuario="";
	File f;
	
	boolean cambio = false;

	public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        
        mc = MensajeChat.getInstance();
        mc.addObserver(this);
        timer.start();
        timer.setRepeats(true);
	}
	
	@Listen("onClick=#btnEnviar")
	public void enviarMensaje() {
		mensaje = txtUsuario.getValue() + ": " + txtMensaje.getValue();		
		mc.setMensaje(mensaje);
		txtMensaje.setValue("");
	}
	
	@Override
	public void update(Observable o, Object mensaje) {
		// TODO Auto-generated method stub
		cambio = true;
		System.out.println("Observando: "+mc.countObservers());
		this.mensaje=mensaje.toString();		
	}
	
	@Listen("onTimer=#timer")
	public void escucharMensaje() {
		if(cambio) {			
			tvHistorial.setValue(tvHistorial.getValue()+mensaje+"\n");
			cambio = false;		
			
			f = new File("C:\\curso\\historial.txt");
			
			try {			
				FileWriter w = new FileWriter(f);
				BufferedWriter bw = new BufferedWriter(w);
				PrintWriter wr = new PrintWriter(bw);
				wr.write(tvHistorial.getValue());	
				wr.append(mensaje);
				//System.out.println(mensaje);
				wr.close();
				bw.close();
			} catch (IOException e) {
				
			}
		}
	}	
}
