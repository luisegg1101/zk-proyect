package chatUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import chatUtil.ConexionCliente;
import chatUtil.MensajeChat;

public class ServidorChat extends Thread {

	private int puerto = 1234;
	private int maximoConexiones = 10; // Maximo de conexiones simultaneas
	private ServerSocket servidor = null;
	private Socket socket = null;
	private MensajeChat mensajes = MensajeChat.getInstance();

	private static ServidorChat instance = null;

	private ServidorChat() {

	}

	public static ServidorChat getInstance() {
		if (instance == null) {
			instance = new ServidorChat();
			instance.start();
		}
		return instance;
	}

	public void run() {
		try {
			// Se crea el serverSocket
			servidor = new ServerSocket(puerto, maximoConexiones);

			// Bucle infinito para esperar conexiones
			while (true) {
				System.out.println("Servidor a la espera de conexiones.");
				socket = servidor.accept();
				System.out.println("Cliente con la IP " + socket.getInetAddress().getHostName() + " conectado.");
				ConexionCliente cc = new ConexionCliente(socket, mensajes);
				cc.start();

			}
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} finally {
			try {
				socket.close();
				servidor.close();
			} catch (IOException ex) {
				System.out.println("Error al cerrar el servidor: " + ex.getMessage());
			}
		}
	}
}
