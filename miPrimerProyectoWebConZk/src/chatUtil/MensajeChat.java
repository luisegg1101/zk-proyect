package chatUtil;

import java.util.Observable;

public class MensajeChat extends Observable{
	 private String mensaje;
	 private static MensajeChat instance= null;
	    
	    private MensajeChat(){
	    }
	    
	    public String getMensaje(){
	        return mensaje;
	    }
	    
	    public static MensajeChat getInstance() {
	    	if(instance==null) {
	    		instance = new MensajeChat();
	    	}
	    	return instance;
	    }
	    
	    public void setMensaje(String mensaje){
	        this.mensaje = mensaje;
	        // Indica que el mensaje ha cambiado
	        this.setChanged();
	        // Notifica a los observadores que el mensaje ha cambiado y se lo pasa
	        // (Internamente notifyObservers llama al metodo update del observador)
	        this.notifyObservers(this.getMensaje());
	    }
}
